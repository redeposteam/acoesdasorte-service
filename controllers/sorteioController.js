var db = require('../models');

module.exports.list = (request, response, fail) => {
    db.sorteio.findAll().then(list => {
        response.send(list);
    })
    .catch(fail);
}