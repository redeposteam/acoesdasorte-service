var db = require('../models');

module.exports.list = (request, response, fail) => {
    db.sorteio.findAll().then(sorteioList => {
        db.assunto.findAll().then(assuntoList => {
            response.send({
                sorteios: sorteioList,
                assuntos: assuntoList
            });
        })
        .catch(fail);
    })
    .catch(fail);
}