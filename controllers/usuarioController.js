var basicAuth = require('basic-auth');
var md5 = require('md5');
var db = require('../models');
const crypto = require('crypto');
var nodemailer = require('nodemailer');
const MESSAGES = require("../config/messages.json");
const config = require("../config/config.json");

function validarUsuario(user, success, fail) {
    db.usuario.findOne({ where: { login: user.name }})
    .then(usuario => {
        if (usuario != null && usuario.senha == md5(user.pass)) {
            if (!usuario.ativo)
                return fail({ status: 403 });
            return success(usuario);
        }
        return fail();
    })
    .catch(fail);
}

module.exports.auth_user = (request, response, next) => {
    var user = basicAuth(request);
    var fail = (err) => {
        if (err == null) {
            response.set('WWW-Authenticate', 'Basic realm=Authorization Required');
            response.sendStatus(401);
        } else if (err.status == 403) {
            response.status(403);
            response.send(MESSAGES.USER_BLOCKED);
        } else {
            next(err);
        }
    };
    if (user == null)
        return fail();
    validarUsuario(user, (usuario) => {
        request.headers.usuario = usuario;
        return next();
    }, fail);
}

module.exports.change_password = (request, response, fail) => {
    var param = request.body;
    var usuario = request.headers.usuario;
    if (usuario.senha != md5(param.senha))
        throw new Error(MESSAGES.WRONG_PASSWORD);
    usuario.senha = md5(param.novaSenha);
    usuario.dataAlteracaoSenha = new Date();
    usuario.save().then(() => response.send()).catch(fail);
}

function encrypt(str) {
    var cipher = crypto.createCipher('aes128', 'S$Wq!d 2');
    str = cipher.update(str, 'utf8', 'base64');
    str += cipher.final('base64');
    str = str.replace(/\+/g, '-').replace(/\//g, '_');
    return str;
}


function decrypt(str) {
    var decipher = crypto.createDecipher('aes128', 'S$Wq!d 2');
    str = str.replace(/\-/g, '+').replace(/_/g, '/');
    str = decipher.update(str, 'base64', 'utf8');
    str += decipher.final('utf8');
    return str;
}

module.exports.recover_password = (request, response, fail) => {
    var param = request.body;
    db.usuario.findOne({ where: { login: param.login } })
        .then(usuario => {
            try {
                if (usuario == null)
                    throw new Error(MESSAGES.USER_NOT_FOUND);
                if (usuario.email != param.email)
                    throw new Error(MESSAGES.WRONG_MAIL);
                var token = {
                    login: usuario.login,
                    date: new Date(),
                    expiresIn: 240
                };
                token = encrypt(JSON.stringify(token));
                var url = 'http://' + request.headers.host + '/usuario/recuperacaoSenha/' + token;
                sendRecoveryMail(usuario.email, url, () => {
                    response.send();
                }, fail);
            } catch (ex) {
                fail(ex);
            }
        })
        .catch(fail);
}

module.exports.recovery_page = (request, response, fail) => {
    response.render('recoveryPassword');
}

module.exports.recovery_page_submit = (request, response, fail) => {
    var token = decrypt(request.params.token);
    token = JSON.parse(token);

    db.usuario.findOne({ where: { login: token.login } })
    .then(usuario => {
        try {
            var tokenDate = new Date(token.date);
            if (usuario != null && (usuario.dataAlteracaoSenha == null || usuario.dataAlteracaoSenha < tokenDate) 
                && (new Date() - tokenDate) / 60000 <= token.expiresIn) {
                usuario.senha = md5(request.body.password);
                usuario.dataAlteracaoSenha = new Date();
                usuario.save().then(() => {
                    response.send(MESSAGES.PASSWORD_CHANGED);
                }).catch(fail);
            } else {
                response.send(MESSAGES.PASSWORD_NOT_CHANGED);
            }
        } catch (ex) {
            fail(ex);
        }
    }).catch(fail);
}

function sendRecoveryMail(email, url, success, fail) {
    var transporter = nodemailer.createTransport({
        service: 'gmail',
        auth: config.mailAccount,
        tls: {
            rejectUnauthorized: false
        }
      });
      
      var mailOptions = {
        from: config.mailAccount.user,
        to: email,
        subject: MESSAGES.RECOVERY_MAIL_SUBJECT,
        text: MESSAGES.RECOVERY_MAIL_BODY + url
      };
      
      transporter.sendMail(mailOptions, function(error, info){
        if (error) {
            fail(new Error(error));
        } else {
            success();
        }
      });
}