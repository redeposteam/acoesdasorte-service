var fs = require('fs');
var db = require('../models');
var config = require('../config/config.json');

module.exports.save = (request, response, fail) => {
    var usuario = request.headers.usuario;
    var midia = request.body;
    db.midia.create({
        arquivo: "",
        dataHora: midia.dataHora,
        latitude: midia.latitude,
        longitude: midia.longitude,
        idUsuario: usuario.id,
        idAssunto: midia.idAssunto,
        idSorteio: midia.idSorteio
    }).then((m) => response.send({ id: m.id })).catch(fail);
}

module.exports.list = (request, response, fail) => {
    var param = request.body;
    db.midia.findAll({ where: {
        idSorteio: param.idSorteio,
        idAssunto: param.idAssunto
    }, include: [db.usuario]}).then(list => {
        if (list.length == 0) {
            response.send([]);
            return;
        }
        var midiaList = [];
        for (var i = 0; i < list.length; i++) {
            var midia = list[i];
            midiaList.push({
                id: midia.id,
                usuario: midia.usuario.nome,
                dataHora: midia.dataHora,
                latitude: midia.latitude,
                longitude: midia.longitude,
                mimeType: midia.mimeType 
            });
        }
        response.send(midiaList);
    }).catch(fail);
}

module.exports.getFile = (request, response, fail) => {
    db.midia.findOne({ where: {
        id: request.params.id
    }, include: [db.usuario]}).then(midia => {
        if (midia == null || midia.arquivo == null || midia.arquivo.trim().length == 0)
            return response.sendStatus(404);
        readFile(midia.arquivo).then(data => {
            try {
                response.writeHead(200, {'Content-Type': midia.mimeType });
                response.end(data, 'binary');
            } catch (ex) {
                fail(ex);
            }
        }).catch(fail);
    }).catch(fail);
}

module.exports.updateFile = (request, response, fail) => {
    db.midia.findOne({ where: {
        id: request.params.id
    }}).then(midia => {
        if (midia == null)
            return response.sendStatus(404);
        var mimeType = request.headers['content-type'];
        var filePath = config.dirMidia + "\\M" + midia.id + mimeType.replace(/.*\//, '.');
        var data = [];
        request.on('data', (buffer) => {
            data.push(buffer);
        });
        request.on('end', () => {
            data = Buffer.concat(data);
            createFile(filePath, data, () => {
                midia.arquivo = filePath;
                midia.mimeType = mimeType;
                midia.save().then(() => response.send()).catch(fail);
            }, fail);
        });   
    }).catch(fail);
}

function readFile(path) {
    return new Promise((success, fail) => {
        fs.readFile(path, "binary", (err, data) => {
            try {
                if (err)
                    throw new Error(err);
                success(data);
            } catch(ex) {
                fail(ex);
            }
        });
    });
}

function createFile(filePath, file, success, fail) {
    fs.writeFile(filePath, file, "binary", (err) => {
        if (err)
            return fail(err);
        success();
    });
}