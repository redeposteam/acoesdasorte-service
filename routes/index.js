var express = require('express');
var router = express.Router();
var usuarioController = require('../controllers/usuarioController');
var sorteioController = require('../controllers/sorteioController');
var assuntoController = require('../controllers/assuntoController');
var midiaController = require('../controllers/midiaController');
var filtroController = require('../controllers/filtroController');
var models = require('../models');

var auth = usuarioController.auth_user;

router.get('/', (request, response) => {
  response.send("ok");
});

router.post('/login', auth, (request, response) => {
  response.send();
});

router.post('/usuario/alterarSenha', auth, usuarioController.change_password);
router.post('/usuario/recuperarSenha', usuarioController.recover_password);
router.get('/usuario/recuperacaoSenha/:token', usuarioController.recovery_page);
router.post('/usuario/recuperacaoSenha/:token', usuarioController.recovery_page_submit);
router.get('/sorteio/listar', auth, sorteioController.list);
router.get('/assunto/listar', auth, assuntoController.list);
router.get('/filtro/listar', auth, filtroController.list);
router.post('/sorteio/listar', auth, sorteioController.list);
router.post('/assunto/listar', auth, assuntoController.list);
router.post('/filtro/listar', auth, filtroController.list);
router.post('/midia/listar', auth, midiaController.list);
router.post('/midia/salvar', auth, midiaController.save);
router.get('/midia/arquivo/:id', auth, midiaController.getFile);
router.post('/midia/arquivo/:id', auth, midiaController.updateFile);

module.exports = router;
