Após o clone rode npm install para baixar as dependências

rests:
/login
/filtro/listar
/sorteio/listar
/assunto/listar
/midia/salvar
/midia/listar
/midia/arquivo/:id - GET retorna a imagem
/midia/arquivo/:id - POST atualiza a imagem
/usuario/alterarSenha
/usuario/recuperarSenha


Exemplos:
/login
Parâmetros (header):
Authorization: Basic amVmZjozMjE=

obs: todos os métodos são autenticados e precisam desse header.


/filtro/listar
Retorno:
{	
  "sorteios": [
    {
      "id": 1,
      "descricao": "sorteio 1"
    }
  ],
  "assuntos": [
    {
      "id": 1,
      "descricao": "assunto 1"
    }
  ]
}


/midia/salvar
Parâmetros (body):
{
	"login": "jeff",
	"latitude": -34.7856456,
	"longitude": -8.023564,
	"idSorteio": 1,
	"idAssunto": 1,
	"dataHora": "2017-10-05 20:18:23"
}

/midia/listar
Parâmetros (body):
{
	"idAssunto": 1,
	"idSorteio": 1
}

Retorno:
[
    {
        "id": 5,
        "usuario": "Jefferson Raposo",
        "dataHora": "2017-10-05T20:18:23.000Z",
        "latitude": -34.7856456,
        "longitude": -8.023564,
        "mimeType": "image/jpeg"
	}
]

/usuario/alterarSenha
Parâmetros (body):
{
	"senha":"123",
	"novaSenha":"123"
}

/usuario/recuperarSenha
Parâmetros (body):
{
	"login": "jeff",
	"email": "jeffersonraposo@gmail.com"
}



tabelas:
create table sorteio (
	id int not null auto_increment, 
	descricao varchar(255) not null, 
	primary key(id)
);
create table assunto (
	id int not null auto_increment, 
	descricao varchar(255) not null, 
	primary key(id)
);
create table usuario (
	id int not null auto_increment, 
	login varchar(50) not null unique,
	email varchar(50) not null unique,
	senha varchar(100) not null,
	nome varchar(255) not null,
	ativo bit not null default 1,
	dataAlteracaoSenha datetime null,
	primary key(id)
);
create table midia (
	id int not null auto_increment,
	arquivo varchar(255) null,
	mimeType varchar(100) null,
	dataHora datetime not null,
	latitude float not null,
	longitude float not null,
	idUsuario int not null,
	idSorteio int not null,
	idAssunto int not null,
	primary key(id),
	foreign key(idUsuario) references usuario(id),
	foreign key(idSorteio) references sorteio(id),
	foreign key(idAssunto) references assunto(id)
);