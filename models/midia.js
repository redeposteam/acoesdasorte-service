module.exports = (seq, DataTypes) => {
    var midia = seq.define("midia", {
        id: { type: DataTypes.INTEGER, primaryKey: true, autoIncrement: true },
        arquivo: DataTypes.STRING,
        dataHora: DataTypes.DATE,
        latitude: DataTypes.STRING,
        longitude: DataTypes.STRING,
        mimeType: DataTypes.STRING
    }, {
        freezeTableName: true
    });

    midia.associate = (models) => {
        midia.belongsTo(models.usuario, { foreignKey: 'idUsuario' });
        midia.belongsTo(models.assunto, { foreignKey: 'idAssunto' });
        midia.belongsTo(models.sorteio, { foreignKey: 'idSorteio' });
    };
    
    return midia;
}