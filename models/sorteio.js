module.exports = (seq, DataTypes) => {
    var sorteio = seq.define("sorteio", {
        id: { type: DataTypes.INTEGER, primaryKey: true },
        descricao: DataTypes.STRING
    }, {
        freezeTableName: true
    });
    return sorteio;
}