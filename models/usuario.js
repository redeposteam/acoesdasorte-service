module.exports = (seq, DataTypes) => {
    var usuario = seq.define("usuario", {
        id: { type: DataTypes.INTEGER, primaryKey: true },
        login: DataTypes.STRING,
        email: DataTypes.STRING,
        senha: DataTypes.STRING,
        nome: DataTypes.STRING,
        ativo: { 
            type: DataTypes.BOOLEAN,
            get() {
                return this.getDataValue('ativo') == true || this.getDataValue('ativo') == 1;
            }
        },
        dataAlteracaoSenha: DataTypes.DATE
    }, {
        freezeTableName: true
    });
    return usuario;
}