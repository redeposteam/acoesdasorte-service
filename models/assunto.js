module.exports = (seq, DataTypes) => {
    var assunto = seq.define("assunto", {
        id: { type: DataTypes.INTEGER, primaryKey: true },
        descricao: DataTypes.STRING
    }, {
        freezeTableName: true
    });
    return assunto;
}